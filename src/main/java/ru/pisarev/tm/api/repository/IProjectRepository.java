package ru.pisarev.tm.api.repository;

import ru.pisarev.tm.api.IBusinessRepository;
import ru.pisarev.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IBusinessRepository<Project> {

    List<Project> findAll(final String userId, final Comparator<Project> comparator);

    Project findByName(final String userId, final String name);

    Project findByIndex(final String userId, final int index);

    Project removeByName(final String userId, final String name);

    Project removeByIndex(final String userId, final int index);

    int getSize(final String userId);
}
