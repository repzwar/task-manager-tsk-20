package ru.pisarev.tm.api.repository;

import ru.pisarev.tm.api.IBusinessRepository;
import ru.pisarev.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    List<Task> findAll(final String userId, final Comparator<Task> comparator);

    List<Task> findAllTaskByProjectId(final String userId, final String projectId);

    void removeAllTaskByProjectId(final String userId, final String projectId);

    Task bindTaskToProjectById(final String userId, final String taskId, final String projectId);

    Task unbindTaskById(final String userId, final String id);

    Task findByName(final String userId, final String name);

    Task findByIndex(final String userId, final int index);

    Task removeByName(final String userId, final String name);

    Task removeByIndex(final String userId, final int index);

    int getSize(final String userId);
}
