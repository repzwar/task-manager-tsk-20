package ru.pisarev.tm.api;

import ru.pisarev.tm.model.AbstractBusinessEntity;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IBusinessRepository<E> {
}
