package ru.pisarev.tm.api.service;

import ru.pisarev.tm.api.IService;
import ru.pisarev.tm.model.User;

public interface IUserService extends IService<User> {

    User findByLogin(final String login);

    User findByEmail(final String email);

    User removeByLogin(final String login);

    User add(final String login, final String password);

    User add(final String login, final String password, final String email);

    User setPassword(final String id, final String password);

    boolean isLoginExist(final String login);

    boolean isEmailExist(final String email);

    User updateUser(
            final String id,
            final String firstName,
            final String lastName,
            final String middleName
    );
}
