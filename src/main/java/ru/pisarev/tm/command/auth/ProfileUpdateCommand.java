package ru.pisarev.tm.command.auth;

import ru.pisarev.tm.command.AuthAbstractCommand;
import ru.pisarev.tm.model.User;
import ru.pisarev.tm.util.TerminalUtil;

public class ProfileUpdateCommand extends AuthAbstractCommand {
    @Override
    public String name() {
        return "update-profile";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update profile.";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter first name");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(user.getId(), firstName, lastName, middleName);
    }
}
