package ru.pisarev.tm.command;

import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;
import ru.pisarev.tm.model.Task;

public abstract class TaskAbstractCommand extends AbstractCommand {

    protected void show(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
        System.out.println("Project Id: " + task.getProjectId());
    }

    protected Task add(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return new Task(name, description);
    }

}
