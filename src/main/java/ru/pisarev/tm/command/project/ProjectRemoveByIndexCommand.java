package ru.pisarev.tm.command.project;

import ru.pisarev.tm.command.ProjectAbstractCommand;
import ru.pisarev.tm.exception.entity.ProjectNotFoundException;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.model.User;
import ru.pisarev.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by index.";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().findByIndex(user.getId(), index);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectTaskService().removeProjectByIndex(user.getId(), index);
    }
}
