package ru.pisarev.tm.command.task;

import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.model.User;
import ru.pisarev.tm.util.TerminalUtil;

public class TaskStartByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-start-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start task by id.";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().startById(user.getId(), id);
        if (task == null) throw new TaskNotFoundException();
    }
}
