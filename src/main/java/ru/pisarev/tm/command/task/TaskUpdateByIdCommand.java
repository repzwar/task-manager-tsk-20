package ru.pisarev.tm.command.task;

import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.model.User;
import ru.pisarev.tm.util.TerminalUtil;

import static ru.pisarev.tm.util.TerminalUtil.incorrectValue;

public class TaskUpdateByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(user.getId(), id);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateById(user.getId(), id, name, description);
        if (taskUpdated == null) incorrectValue();
    }
}
