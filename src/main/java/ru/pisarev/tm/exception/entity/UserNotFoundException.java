package ru.pisarev.tm.exception.entity;

import ru.pisarev.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error. User not found.");
    }

}
