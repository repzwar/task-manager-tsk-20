package ru.pisarev.tm.exception.entity;

import ru.pisarev.tm.exception.AbstractException;

public class LoginExistException extends AbstractException {

    public LoginExistException() {
        super("Error. Login already exist.");
    }

    public LoginExistException(String value) {
        super("Error. Login '" + value + "' already exist.");
    }

}
