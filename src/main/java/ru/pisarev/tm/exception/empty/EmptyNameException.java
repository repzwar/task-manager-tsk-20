package ru.pisarev.tm.exception.empty;

import ru.pisarev.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error. Name is empty");
    }

}
