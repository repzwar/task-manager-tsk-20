package ru.pisarev.tm.model;

public abstract class AbstractBusinessEntity extends AbstractEntity {

    protected String userId = "";

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
