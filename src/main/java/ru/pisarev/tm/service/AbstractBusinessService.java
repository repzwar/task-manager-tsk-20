package ru.pisarev.tm.service;

import ru.pisarev.tm.api.IBusinessRepository;
import ru.pisarev.tm.api.IBusinessService;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.model.AbstractBusinessEntity;

import java.util.Collection;
import java.util.List;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    private final IBusinessRepository<E> repository;

    public AbstractBusinessService(final IBusinessRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public List<E> findAll(final String userId) {
        return repository.findAll(userId);
    }

    @Override
    public void addAll(final String userId, final Collection<E> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.addAll(userId, collection);
    }

    @Override
    public E add(final String userId, final E entity) {
        if (entity == null) return null;
        return repository.add(userId, entity);
    }

    @Override
    public E findById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Override
    public void clear(final String userId) {
        repository.clear(userId);
    }

    @Override
    public E removeById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(userId, id);
    }

    @Override
    public E remove(final String userId, final E entity) {
        if (entity == null) return null;
        return repository.remove(userId, entity);
    }

}
