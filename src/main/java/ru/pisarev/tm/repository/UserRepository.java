package ru.pisarev.tm.repository;

import ru.pisarev.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements ru.pisarev.tm.api.repository.IUserRepository {

    @Override
    public User findByLogin(final String login) {
        for (User user : entities.values()) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (User user : entities.values()) {
            if (email.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeUserByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        entities.remove(user.getId());
        return user;
    }

}
