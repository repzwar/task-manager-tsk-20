package ru.pisarev.tm.repository;

import ru.pisarev.tm.api.repository.ITaskRepository;
import ru.pisarev.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAll(final String userId, final Comparator<Task> comparator) {
        final List<Task> tasks = new ArrayList<>(findAll(userId));
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String userId, final String projectId) {
        List<Task> entitiesByProject = new ArrayList<>();
        for (Task task : entities.values()) {
            if (!userId.equals(task.getUserId())) continue;
            if (projectId.equals(task.getProjectId())) entitiesByProject.add(task);
        }
        return entitiesByProject;
    }

    @Override
    public void removeAllTaskByProjectId(final String userId, final String projectId) {
        List<Task> entitiesByProject = findAllTaskByProjectId(userId, projectId);
        for (Task entity : entitiesByProject) {
            this.entities.remove(entity.getId());
        }
    }

    @Override
    public Task bindTaskToProjectById(final String userId, final String taskId, final String projectId) {
        final Task task = findById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setProjectId("");
        return task;
    }

    @Override
    public Task findByName(final String userId, final String name) {
        for (Task task : entities.values()) {
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final String userId, final int index) {
        List<Task> entities = findAll(userId);
        return entities.get(index);
    }

    @Override
    public Task removeByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        entities.remove(task.getId());
        return task;
    }

    @Override
    public Task removeByIndex(final String userId, final int index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        entities.remove(task.getId());
        return task;
    }

    @Override
    public int getSize(final String userId) {
        List<Task> entities = findAll(userId);
        return entities.size();
    }

}
