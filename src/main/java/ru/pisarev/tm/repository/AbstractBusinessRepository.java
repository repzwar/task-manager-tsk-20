package ru.pisarev.tm.repository;

import ru.pisarev.tm.model.AbstractBusinessEntity;

import java.util.*;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> {

    protected final Map<String, E> entities = new LinkedHashMap<>();

    public List<E> findAll(final String userId) {
        List<E> entities = new ArrayList<>();
        for (E entity : this.entities.values()) {
            if (userId.equals(entity.getUserId())) entities.add(entity);
        }
        return entities;
    }

    public void addAll(final String userId, final Collection<E> collection) {
        if (collection == null) return;
        for (E entity : collection) {
            entity.setUserId(userId);
            entities.put(entity.getId(), entity);
        }
    }

    public E add(final String userId, final E entity) {
        if (entity == null) return null;
        entity.setUserId(userId);
        entities.put(entity.getId(), entity);
        return entity;
    }

    public E findById(final String userId, final String id) {
        E entity = entities.get(id);
        if (entity == null) return null;
        if (userId.equals(entity.getId())) return entity;
        return null;
    }

    public void clear(final String userId) {
        List<E> entities = findAll(userId);
        for (E entity : entities) {
            this.entities.remove(entity.getId());
        }
    }

    public E removeById(final String userId, final String id) {
        final E entity = findById(userId, id);
        if (entity == null) return null;
        entities.remove(entity.getId());
        return entity;
    }

    public E remove(final String userId, final E entity) {
        if (!userId.equals(entity.getUserId())) return null;
        entities.remove(entity.getId());
        return entity;
    }

}
