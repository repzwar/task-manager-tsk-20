package ru.pisarev.tm.repository;

import ru.pisarev.tm.api.IRepository;
import ru.pisarev.tm.model.AbstractEntity;

import java.util.*;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final Map<String, E> entities = new LinkedHashMap<>();

    @Override
    public List<E> findAll() {
        return new ArrayList<>(entities.values());
    }

    @Override
    public void addAll(final Collection<E> collection) {
        if (collection == null) return;
        for (E entity : collection) {
            entities.put(entity.getId(), entity);
        }
    }

    @Override
    public E add(final E entity) {
        if (entity == null) return null;
        entities.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public E findById(final String id) {
        return entities.get(id);
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public E removeById(final String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        entities.remove(id);
        return entity;
    }

    @Override
    public E remove(final E entity) {
        entities.remove(entity.getId());
        return entity;
    }
}
